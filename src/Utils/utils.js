const utils = {
    CalculatorOperations:  {
        '/': (prevValue, nextValue) => prevValue / nextValue,
        '*': (prevValue, nextValue) => prevValue * nextValue,
        '+': (prevValue, nextValue) => prevValue + nextValue,
        '-': (prevValue, nextValue) => prevValue - nextValue,
        '=': (prevValue, nextValue) => nextValue
    },

    CalculateScientificOperations: {
        'sqrt': val => Math.sqrt(val),
        'sqr': val => val * val
    }
} 

export default utils;
