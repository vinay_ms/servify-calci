import React, { Component } from 'react';
import Key from "../Key/Key";
import Display from '../Display/Display';
import utils from '../../Utils/utils';

class Calculator extends Component {
    state = {
        value: null,
        displayValue: '0',
        operator: null,
        waitingForOperand: false,
        showScientific: false,
    };

    clearAll() {
        this.setState({
            value: null,
            displayValue: '0',
            operator: null,
            waitingForOperand: false
        })
    }

    clearDisplay() {
        this.setState({
            displayValue: '0'
        })
    }

    clearLastChar() {
        const { displayValue } = this.state

        this.setState({
            displayValue: displayValue.substring(0, displayValue.length - 1) || '0'
        })
    }

    toggleSign() {
        const { displayValue } = this.state
        const newValue = parseFloat(displayValue) * -1

        this.setState({
            displayValue: String(newValue)
        })
    }

    inputDigit(digit) {
        const { displayValue, waitingForOperand } = this.state

        if (waitingForOperand) {
            this.setState({
                displayValue: String(digit),
                waitingForOperand: false
            })
        } else {
            this.setState({
                displayValue: displayValue === '0' ? String(digit) : displayValue + digit
            })
        }
    }

    /**
     * Performs addition, subtraction, division, multiplication
     * @function
     * @param {String} nextOperator is the pressed operator(+, -, *, /, =)
     * @function CalculatorOperations from utils performs operation
     * displays the result
     * sets new value for next operations
     */
    performOperation(nextOperator) {
        const { value, displayValue, operator } = this.state
        const inputValue = parseFloat(displayValue)

        if (value == null) {
            this.setState({
                value: inputValue
            })
        } else if (operator) {
            const currentValue = value || 0
            const newValue = utils.CalculatorOperations[operator](currentValue, inputValue)

            this.setState({
                value: newValue,
                displayValue: String(newValue)
            })
        }

        this.setState({
            waitingForOperand: true,
            operator: nextOperator
        })
    }


    /**
     * 
     * @param {String} operator is 'sqrt' or 'sqr
     * Async function waits for any previous operations if pending and then performs square root or square operation
     * Might be little confusing as this performs sqr or sqrt on previous operation's result
     * Eg: '5' + '5' and sqr will give you 100 and not 25 as 5 + 5 = 10 and 10sqr = 100
     * @function CalculateScientificOperations from utils performs operation
     * displays new value
     */
    async performScientificOperation(operator) {
        await this.performOperation('=');
        const { displayValue } = this.state

        const newVal = utils.CalculateScientificOperations[operator](displayValue);
        this.setState({
            value: newVal,
            displayValue: String(newVal)
        })
    }

    /**
     * 
     * @param {Event} event keydown event when keyboard is used
     * Performs operations on keydown
     * Clears last char when backspace is pressed
     * Dosen't perform scientific operations 
     */

    handleKeyDown = (event) => {
        let { key } = event

        if (key === 'Enter')
            key = '='

        if ((/\d/).test(key)) {
            event.preventDefault()
            this.inputDigit(parseInt(key, 10))
        } else if (key in utils.CalculatorOperations) {
            event.preventDefault()
            this.performOperation(key)
        } else if (key === 'Backspace') {
            event.preventDefault()
            this.clearLastChar()
        } else if (key === 'Clear') {
            event.preventDefault()
            if (this.state.displayValue !== '0') {
                this.clearDisplay()
            } else {
                this.clearAll()
            }
        }
    };


    scientificCalciDisplay = () => {
        let { showScientific } = this.state

        this.setState({ showScientific: !showScientific })
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleKeyDown)
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    render() {
        const { displayValue } = this.state

        const clearDisplay = displayValue !== '0'
        const clearText = clearDisplay ? 'C' : 'AC'

        return (
            <div className="calculator">
                <Display value={displayValue} />
                <div className="calculator-keypad">
                    <div className="input-keys">
                        <Key onClick={() => this.inputDigit(1)}>1</Key>
                        <Key onClick={() => this.inputDigit(2)}>2</Key>
                        <Key onClick={() => this.inputDigit(3)}>3</Key>
                        <Key onClick={() => this.performOperation('+')}>+</Key><br/>
                        <Key onClick={() => this.inputDigit(4)}>4</Key>
                        <Key onClick={() => this.inputDigit(5)}>5</Key>
                        <Key onClick={() => this.inputDigit(6)}>6</Key>
                        <Key onClick={() => this.performOperation('-')}>−</Key><br/>
                        <Key onClick={() => this.inputDigit(7)}>7</Key>
                        <Key onClick={() => this.inputDigit(8)}>8</Key>
                        <Key onClick={() => this.inputDigit(9)}>9</Key>
                        <Key onClick={() => this.performOperation('*')}>×</Key><br/>
                        <Key onClick={() => clearDisplay ? this.clearDisplay() : this.clearAll()}>{clearText}</Key>
                        <Key onClick={() => this.performOperation('=')}>=</Key>
                        <Key onClick={() => this.inputDigit(0)}>0</Key>
                        <Key onClick={() => this.performOperation('/')}>÷</Key><br/>
                        <Key onClick={() => this.scientificCalciDisplay()}>scientific</Key>
                        {
                            this.state.showScientific ? 
                            <div className="scientific-buttons">
                                    <Key onClick={() => this.performScientificOperation('sqrt')}>Sqrt</Key>
                                <Key onClick={() => this.toggleSign()}>+/-</Key>
                                    <Key onClick={() => this.performScientificOperation('sqr')}>Sqr</Key>
                            </div> :
                            null
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default Calculator;