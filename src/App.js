import React, { useState } from 'react';
import Calculator from './Components/Calculator/Calculator';
import './App.css';

const App = (props) => {
  const [theme, setTheme ] = useState('light');
  return (
    <div className={`calculator-wrapper ${theme}`}>
      <div className="calci-app">
        <div className="choose-theme">
          <button className={`theme-btn dark-btn ${theme === 'dark' ? 'clicked' : ''}`} onClick={() => setTheme('dark')}>Dark</button>
          <button className={`theme-btn light-btn ${theme === 'light' ? 'clicked' : ''}`} onClick={() => setTheme('light')}>Light</button>
        </div>
        <div className="calculator-body">
          <h1>Calculator</h1>
          <Calculator />
        </div>
      </div>
    </div>
  ); 
}

export default App;
